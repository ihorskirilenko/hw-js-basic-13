'use strict'

/**
 * Теорія:
 * 1. Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
 * setTimeout() призначена для відкладеного виклику вкладеної функції, тобто функція, що передається першим аргументом буде виконана
 * із затримкою у часі, визначеною у другому аргументі;
 * setInterval() призначена для умовно безкінечного запуску вкладеної фукції з інтервалом виконання, визначеному у другому аргументі.
 *
 * 2. Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
 * Так, спрацює. Умовно миттєво (тобто із затримкою в 0 мілісекунд), але пріоритет виконання браузером у функції, що передана
 * у setTimeout з нульовою затримкою буде нижче, ніж у функції без такої "обгортки", навіть якщо інша функція буде знаходитись далі в коді.
 *
 * 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
 * Для того, щоб зупинити ініційований безкінечний цикл, який в інакшому разі буде без потреби використовувати ресурси.
 *
 * Завдання
 * Реалізувати програму, яка циклічно показує різні картинки. Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
 *
 * Технічні вимоги:
 * У папці banners лежить HTML код та папка з картинками.
 * При запуску програми на екрані має відображатись перша картинка.
 * Через 3 секунди замість неї має бути показано друга картинка.
 * Ще через 3 секунди – третя.
 * Ще через 3 секунди – четверта.
 * Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
 * Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
 * Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
 * Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
 * Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
 * Необов'язкове завдання підвищеної складності
 * При запуску програми на екрані повинен бути таймер з секундами та мілісекундами, що показує, скільки залишилося до показу наступної картинки.
 * Робити приховування картинки та показ нової картинки поступовим (анімація fadeOut/fadeIn) протягом 0.5 секунди. *
 * */

window.addEventListener('load', () => {

    function createNav() {
        let nav = document.createElement('div');
        nav.style.cssText = `display: flex; width: 400px`;
        nav.insertAdjacentHTML('afterbegin',
            '<button type="button" id="start" style="margin: 5px">Start slideshow</button>' +
            '<button type="button" id="stop" style="margin: 5px">Stop slideshow</button>' +
            '<div id="timer" style="margin: 5px">Timer would be here</div>');
        document.body.prepend(nav);
    }
    createNav();

    const images = document.querySelectorAll('.image-to-show');
    let duration = 3000;

    sessionStorage.setItem('currImg', '0');
    let currImg = sessionStorage.getItem('currImg');

    let tm01;
    let tm02;
    let go;
    let isShowRun;

    function slideShow(dur, elem = 0) {
        isShowRun = true;
        go = setTimeout(function showImg(elem) {
            images.forEach(el => {
                el.classList.contains('active') ? el.classList.remove('active') : false;
                el.style.opacity = '0';
            })
            images[elem].classList.add('active');

            countDown(duration);
            fadeIn(images[elem]);
            tm01 = setTimeout(fadeOut, dur - 500, images[elem]);

            currImg < images.length - 1 ? currImg++ : currImg = 0;
            go = setTimeout(showImg, duration, currImg);
        }, 0, currImg);
    }

    slideShow(duration);

    function fadeIn(el) {
        let fadeIn = setInterval(() => {
            +el.style.opacity < 1  && isShowRun ? el.style.opacity = `${+el.style.opacity + 0.1}` : clearInterval(fadeIn);
        }, 50)
    }

    function fadeOut(el) {
        let fadeOut = setInterval(() => {
            +el.style.opacity > 0 && isShowRun ? el.style.opacity = `${+el.style.opacity - 0.1}` : clearInterval(fadeOut);
        }, 50)
    }

    function countDown(dur) {
        tm02 = setInterval(() => {
            if (dur < 0) {
                dur = duration;
            } else {
                dur -= 5;
                document.querySelector('#timer').innerHTML = `${dur / 1000}`;
            }
        }, 5);
        setTimeout(clearInterval, dur, tm02); /*suicide!*/
    }

    document.querySelector('#start').addEventListener('click', function startShow() {
        isShowRun ? false : slideShow(duration, currImg);

    })

    document.querySelector('#stop').addEventListener('click', function stopShow() {
        isShowRun = false;
        clearInterval(tm01);
        images.forEach(el => { el.classList.contains('active') ? false : el.style.opacity = '0' ; })
        clearInterval(tm02);
        clearInterval(go);
    })
})

























